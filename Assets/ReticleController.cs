﻿using UnityEngine;

/// <summary>
/// Author: Alex Bethke
/// 
/// Provides some reticle and targeting control following a couple of rules.
/// 1) Lock on occurs when the target enters the reticle.
/// 2) Once locked on the reticle will follow the target around the screen.
/// 3) If locked on and the target moves up the camera (Main Camera) will tilt to track the target within configured ranges.
/// 4) If the target moves off screen the reticle collapses to a directional arrow, with a configurable margin for screen bounds.
/// 5) If the target moves behind the camera the lock on breaks.
/// 
/// </summary>
public class ReticleController : MonoBehaviour
{
    void Start()
    {
        CreatePips();
    }

    private void CreatePips()
    {
        _pipTargets = new Vector3[numPips];
        _pips = new GameObject[numPips];

        for (int i = 0; i < numPips; i++)
        {
            GameObject pip = Instantiate(pipPrefab, canvas.transform);
            _pips[i] = pip;
        }
    }

    void Update()
    {
        AdjustCameraRotation();
        DetermineCurrentState();

        switch (_mode)
        {
            case Modes.NONE:
            case Modes.OFF_SCREEN:
            case Modes.ON_SCREEN:
                UpdateTargetsForNoLock();
                MovePipsForNoLock();
            break;
            case Modes.OFF_SCREEN_TARGETED:
                UpdateTargetsForOffScreenLockedOn();
                MovePipsForLockedOn();
            break;
            case Modes.ON_SCREEN_TARGETED:
                UpdateTargetsForOnScreenLockedOn();
                MovePipsForLockedOn();
            break;
        }
    }

    #region Camera handling
    /// <summary>
    /// Handles moving the camera within the min/max angles on the x axis to keep the target in frame
    /// </summary>
    private void AdjustCameraRotation()
    {
        Vector2 screenTarget = Camera.main.WorldToViewportPoint(GetTargetPositionInWorld());
        if (screenTarget.x < 0 || screenTarget.x > 1)
        {
            return;
        }

        Quaternion lookRotation = Quaternion.LookRotation(GetTargetPositionInWorld() - Camera.main.transform.position);

        float x = lookRotation.eulerAngles.x;
        if (x >= 180)
        {
            // puts the angles between -180 and 180 which negates the 360/0 wraparound problem
            x -= 360;
            x = Mathf.Clamp(x, minCameraAngle, maxCameraAngle);
        } else
        {
            x = Mathf.Clamp(x, minCameraAngle, maxCameraAngle);
        }

        Camera.main.transform.eulerAngles = new Vector3(x, Camera.main.transform.eulerAngles.y, Camera.main.transform.eulerAngles.z);
    }
    #endregion Camera handling

    #region Target logic
    private void DetermineCurrentState()
    {
        Vector2 targetPositionOnScreen = GetTargetPositionOnScreen();
        if (IsTargetOnScreen())
        {
            if (Vector2.Distance(targetPositionOnScreen, new Vector2(Screen.width * 0.5f, Screen.height * 0.5f)) < lockOnDistance || _mode == Modes.ON_SCREEN_TARGETED || _mode == Modes.OFF_SCREEN_TARGETED)
            {
                _mode = Modes.ON_SCREEN_TARGETED;
            }
            else
            {
                _mode = Modes.ON_SCREEN;
            }
        }
        else
        {
            if (breakLockOnWhenTargetIsBehind && !IsTargetInFront())
            {
                _mode = Modes.OFF_SCREEN;
            }

            if (_mode == Modes.ON_SCREEN_TARGETED)
            {
                _lockedTargetWentOffScreenAt = Time.time;
                _mode = Modes.OFF_SCREEN_TARGETED;
            }
            else if (_mode == Modes.OFF_SCREEN_TARGETED)
            {
                if (Time.time - _lockedTargetWentOffScreenAt > timeOffScreenBeforeTargetLockBreaks)
                {
                    _mode = Modes.OFF_SCREEN;
                }
            }
            else
            {
                _mode = Modes.NONE;
            }
        }
    }
    #endregion Target logic

    #region Pip position calculators
    private void UpdateTargetsForOnScreenLockedOn()
    {
        Vector2 screenTarget = GetTargetPositionOnScreen();
        float angleIncrement = 360 / numPips;
        float timeOffset = Time.time * pipRotationSpeed;
        for (int i = 0; i < numPips; i++)
        {
            _pipTargets[i] = screenTarget + AngleToXY(timeOffset + i * angleIncrement, pipRotationRadiusForLockedOn);
        }
    }

    private void UpdateTargetsForNoLock()
    {
        Vector2 screenTarget = new Vector2(Screen.width * 0.5f, Screen.height * 0.5f);
        float angleIncrement = 360 / numPips;
        for (int i = 0; i < numPips; i++)
        {
            _pipTargets[i] = screenTarget + AngleToXY(i * angleIncrement, lockOnDistance);
        }
    }

    private void UpdateTargetsForOffScreenLockedOn()
    {
        float margin = 50;
        Vector2 screenTarget = GetTargetPositionOnScreen();
        screenTarget.x = Mathf.Clamp(screenTarget.x, margin, Screen.width - margin);
        screenTarget.y = Mathf.Clamp(screenTarget.y, margin, Screen.height - margin);

        for (int i = 0; i < numPips; i++)
        {
            _pipTargets[i] = screenTarget;
        }
    }
    #endregion Pip position calculators

    #region Pip Movement
    private void MovePipsForLockedOn()
    {
        Vector2 targetPositionOnScreen = GetTargetPositionOnScreen();
        for (int i = 0; i < numPips; i++)
        {
            GameObject pip = _pips[i];
            Vector3 pipTarget = _pipTargets[i];

            float angleFromArrowToTarget = Mathf.Atan2(targetPositionOnScreen.y - pipTarget.y, targetPositionOnScreen.x - pipTarget.x) * (180 / Mathf.PI);

            pip.transform.position = pip.transform.position + (pipTarget - pip.transform.position) / 3f;
            pip.transform.eulerAngles = new Vector3(0, 0, angleFromArrowToTarget + pipRotationBaseAngle);
        }
    }

    private void MovePipsForNoLock()
    {
        Vector2 targetPositionOnScreen = GetTargetPositionOnScreen();
        for (int i = 0; i < numPips; i++)
        {
            GameObject pip = _pips[i];
            Vector3 pipTarget = _pipTargets[i];

            float angleFromArrowToTarget = Mathf.Atan2(targetPositionOnScreen.y - pipTarget.y, targetPositionOnScreen.x - pipTarget.x) * (180 / Mathf.PI);


            pip.transform.position = pip.transform.position + (pipTarget - pip.transform.position) / 10f;
            pip.transform.eulerAngles = new Vector3(0, 0, angleFromArrowToTarget + pipRotationBaseAngle);
        }
    }
    #endregion Pip Movement

    #region Utilities
    static public Vector2 AngleToXY(float in_angleInDegrees, float in_radius)
    {
        Vector2 offsetPosition = Vector2.zero;
        offsetPosition.x += in_radius * Mathf.Sin(in_angleInDegrees * Mathf.Deg2Rad);
        offsetPosition.y += in_radius * Mathf.Cos(in_angleInDegrees * Mathf.Deg2Rad);
        return offsetPosition;
    }

    private Vector3 GetTargetPositionInWorld()
    {
        return target.transform.position + Vector3.up * 0.5f;
    }

    private Vector2 GetTargetPositionOnScreen()
    {
        return Camera.main.WorldToScreenPoint(GetTargetPositionInWorld());
    }

    private bool IsTargetOnScreen()
    {
        bool isTargetOnScreen = true;
        Vector2 screenTarget = Camera.main.WorldToViewportPoint(GetTargetPositionInWorld());
        if (screenTarget.x < 0 || screenTarget.x > 1 || screenTarget.y < 0 || screenTarget.y > 1)
        {
            isTargetOnScreen = false;
        } else if (!IsTargetInFront())
        {
            isTargetOnScreen = false;
        }
        return isTargetOnScreen;
    }

    private bool IsTargetInFront()
    {
        Vector3 heading = GetTargetPositionInWorld() - Camera.main.transform.position;
        float dotProduct = Vector3.Dot(heading, Camera.main.transform.forward);
        if (dotProduct < 0)
        {
            return false;
        } else
        {
            return true;
        }
    }

    #endregion Utilities

    public enum Modes
    {
        NONE,
        ON_SCREEN,
        ON_SCREEN_TARGETED,
        OFF_SCREEN,
        OFF_SCREEN_TARGETED
    }

    private Modes _mode = Modes.NONE;

    private GameObject[] _pips;
    private Vector3[] _pipTargets;
    private Vector3[] _pipRotations;

    private float _lockedTargetWentOffScreenAt;

    [Header("Configuration")]
    public int numPips = 10;
    public float pipRotationSpeed = 50;
    public float pipRotationRadiusForLockedOn = 125;
    public float pipRotationBaseAngle = 30;
    [Space(20)]
    public float minCameraAngle = -15f;
    public float maxCameraAngle = 0;
    [Space(20)]
    public float lockOnDistance = 200;
    public float timeOffScreenBeforeTargetLockBreaks = 2.3f;
    public bool breakLockOnWhenTargetIsBehind = true;

    [Header("References")]
    [Tooltip("This is where the pips will be created.")]
    public Canvas canvas;
    public GameObject pipPrefab;
    [Space(20)]
    [Tooltip("Set this reference to whatever you want tracked.")]
    public GameObject target;
}
