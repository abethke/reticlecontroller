﻿using UnityEngine;

public class SpinIt : MonoBehaviour
{
    private void Start()
    {
        _startedAt = 0;
        if (useLocal)
        {
            _angles = transform.localEulerAngles;
        } else
        {
            _angles = transform.eulerAngles;
        }
    }

    private void FixedUpdate ()
    {
        float stopTime = _startedAt + stopSpinAfter;
        if (stopSpinAfter > 0 && Time.time > stopTime)
        {
            return;
        }

        if (axis == SPIN_AXIS.x || axis == SPIN_AXIS.xy || axis == SPIN_AXIS.xz || axis == SPIN_AXIS.xyz)
        {
            _angles.x += speed;
        }
        //
        if (axis == SPIN_AXIS.y || axis == SPIN_AXIS.xy || axis == SPIN_AXIS.yz || axis == SPIN_AXIS.xyz)
        {
            _angles.y += speed;
        }
        //
        if (axis == SPIN_AXIS.z || axis == SPIN_AXIS.xz || axis == SPIN_AXIS.yz || axis == SPIN_AXIS.xyz)
        {
            _angles.z += speed;
        }

        if (useLocal)
        {
            transform.localEulerAngles = _angles;
        } else
        {
            transform.eulerAngles = _angles;
        }
    }

    public enum SPIN_AXIS
    {
        x,
        y,
        z,
        xy,
        xz,
        yz,
        xyz
    }

    private float _startedAt;

    private Vector3 _angles;

    [Header("Configuration")]
    public SPIN_AXIS axis;
    public float speed = 1f;
    public bool useLocal;
    [Space(20)]
    public float stopSpinAfter;
}
